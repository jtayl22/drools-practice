package com.sample;

import java.util.Date;

public class Customer {	
	
	private String name;
	private Date birthday;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	
//	public boolean isBirthday() {
//	Date today = new Date();
//	
//	return today.getDate() == birthday.getDate() && today.getMonth() == birthday.getMonth();
//	}
	
	public Customer(String name, Date birthday) {
		super();
		this.name = name;
		this.birthday = birthday;
	}
}
